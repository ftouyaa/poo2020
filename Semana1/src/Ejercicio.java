
public class Ejercicio {

	public static void main(String[] args) { //MAIN
		// TODO Auto-generated method stub

		//TIPOS PRIMITIVOS
		int numEntero = 9;
		double numFlotante = 4.5;
		char caracter = 'A';
		
		//IF
		//OPERADORES LOGICOS
		//IMPRIMIR EN CONSOLA
		if (numEntero < 10) {
			System.out.println("el numero es menor a 10");
		}else {
			System.out.println("el numero es mayor a 10");
		}
		
		//IF CORTO
		System.out.println(numEntero > numFlotante? "entero > flotante" : "entero <= flotante");
		
		//ARREGLO ESTATICO
		int[] arregloEnteros = {1, 6, 3, 7, 5};
		
		//WHILE
		int i = 0;
		while(i < arregloEnteros.length) {
			System.out.println("entero: "+ arregloEnteros[i]);
			i ++;
		}
		
		double[] arregloFlotantes = {1.2, 1.7, 2.3, 5.3, 4.9};
		
		//DO WHILE
		int j = 4;
		do {
			arregloFlotantes[j] = j;
			System.out.println("flotante: "+ arregloFlotantes[j]);
			j--;
		}
		while(j >= 0);
		
		//SWITCH
		switch (caracter) {
		case 'A':
			System.out.println("El valor de caracter es A");
			break;
		case 'B':
			System.out.println("El valor de caracter es B");
			break;
		default:
			throw new IllegalArgumentException("Uncexpected value: " + caracter);
		}
			
		//FINAL
		final char[] arregloCaracteres = {'a', 'b', 'f', 'r'};
		
		//FOR
		for (char k = 0; k < arregloCaracteres.length; k++) {
			System.out.println("en for: " + arregloCaracteres[k]);
		}
		
		
		
	}

}

/*Esta todo correcto*/
